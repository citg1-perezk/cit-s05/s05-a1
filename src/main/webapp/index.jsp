<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import= "java.time.format.*" import="java.time.*" 
    import="java.util.Date" import="java.util.TimeZone" import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Create Static Content</title>
</head>
<body>
<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->

	<%LocalDateTime dateTime1= LocalDateTime.now();
	DateTimeFormatter formatNow=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
	LocalDateTime japan= dateTime1.plusHours(1);
	LocalDateTime germany=dateTime1.minusHours(7);
	%>
	
	<%
		String formatted=dateTime1.format(formatNow); 
		String japan_dateTime= japan.format(formatNow);
		String germany_dateTime= germany.format(formatNow); 
	%>
	
	<h1>Our Date and Time now is...</h1>
		<ul>
			<li>Manila: <%=formatted%></li>
			<li>Japan: <%=japan_dateTime%></li>
			<li>Germany: <%=germany_dateTime%></li>
		</ul>
	
		<%!
			private int InitVar=0;
			private int ServiceVar=0;
			private int DestroyVar=0;
		%>	
		<%!
			public void jspInit(){
			InitVar++;
			System.out.println("jspInit(): init "+ InitVar);
			}
			public void jspDestroy(){
			DestroyVar++;
			System.out.println("jspDestroy(): destroy "+ DestroyVar);
			}
		%>
		<%
			ServiceVar++;
			System.out.println("jspService(): Service "+ ServiceVar);
			String content1= "content1: "+ InitVar;
			String content2="content2: "+ ServiceVar ;
			String content3="content3: "+ DestroyVar ;
		%>
		<h1>JSP</h1>
		<p><%= content1%></p>
		<p><%= content2%></p>
		<p><%= content3%></p>
		
		
</body>
</html>